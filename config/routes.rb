Gifgovern::Application.routes.draw do
    resources :comments
    resources :tags
    resources :ip_actions
    
    #devise_for :users
    
    devise_for :users, controllers: { omniauth_callbacks: "omniauth_callbacks", registrations: 'registrations' }
    
    root :to => "gifs#index"
    match '/gifs/navigate_left/:gif_offset' => 'gifs#navigate_left'
    match '/gifs/navigate_right/:gif_offset' =>'gifs#navigate_right'
    
    match '/gifs/iframe/:id' => 'gifs#iframe'
    match '/gifs/reverse/:id' => 'gifs#reverse'
    match '/gifs/latest' => 'gifs#latest'
    match '/gifs/random' => 'gifs#random'
    
    resources :gifs
    #match '/t/:tag', :controller => 'gifs_of_tag', :action => 'index'
    match 't/:tag' => 'gifs_of_tag#index'
    match 't/:tag/:id' => 'gifs_of_tag#show'
    resources :gifs_of_tag, :path => 't'
    
    resources :votes
    
    match 'special/whoarewe', :controller => 'special', :action => 'howarewe'
    match 'special/policy', :controller => 'special', :action => 'termsOfServices'
    match 'special/howto', :controller => 'special', :action => 'howto'
    match 'special/alert', :controller => 'special', :action => 'alert'
    match 'userspace', :controller => 'special', :action => 'userspace'
    match 'userspace/:username' => 'special#userspace'
    match 'special/redirection', :controller => 'special', :action => 'redirection'
    
    match 'player' => 'special#player'
    
    # The priority is based upon order of creation:
    # first created -> highest priority.
    
    # Sample of regular route:
    #   match 'products/:id' => 'catalog#view'
    # Keep in mind you can assign values other than :controller and :action
    
    # Sample of named route:
    #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
    # This route can be invoked with purchase_url(:id => product.id)
    
    # Sample resource route (maps HTTP verbs to controller actions automatically):
    #   resources :products
    
    # Sample resource route with options:
    #   resources :products do
    #     member do
    #       get 'short'
    #       post 'toggle'
    #     end
    #
    #     collection do
    #       get 'sold'
    #     end
    #   end
    
    # Sample resource route with sub-resources:
    #   resources :products do
    #     resources :comments, :sales
    #     resource :seller
    #   end
    
    # Sample resource route with more complex sub-resources
    #   resources :products do
    #     resources :comments
    #     resources :sales do
    #       get 'recent', :on => :collection
    #     end
    #   end
    
    # Sample resource route within a namespace:
    #   namespace :admin do
    #     # Directs /admin/products/* to Admin::ProductsController
    #     # (app/controllers/admin/products_controller.rb)
    #     resources :products
    #   end
    
    # You can have the root of your site routed with "root"
    # just remember to delete public/index.html.
    # root :to => 'welcome#index'
    
    # See how all your routes lay out with "rake routes"
    
    # This is a legacy wild controller route that's not recommended for RESTful applications.
    # Note: This route will make all actions in every controller accessible via GET requests.
    # match ':controller(/:action(/:id))(.:format)'
end
