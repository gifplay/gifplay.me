#! /bin/sh
DEPLOY_ROOT="/home/deployprd/workspace/releases"
GIT_REPO="https://courriertester:0871236507@git.assembla.com/whatisthatfilm.gifgovern.git"
CURRENT_DATE=`date +%Y%m%d%H%M`
APP_NAME="gifgovern_"$CURRENT_DATE
GIF_DIR="/home/deployprd/data/i"
CURRENT="/home/deployprd/workspace/current"
#1) git pull 
echo "1) Clone project form $GIT_REPO to $APP_NAME"
git clone $GIT_REPO $DEPLOY_ROOT"/"$APP_NAME
echo "1) done"
#2) create link symbolic for gif/sys
echo "2) Create symbolic link of $GIF_DIR to $DEPLOY_ROOT/$APP_NAME/public/gifs"
ln -s $GIF_DIR $DEPLOY_ROOT"/"$APP_NAME/public/i; 
echo "2) done"
#3) remove symb link of current, recreate symb link
echo "3) Remove older symbolic link, Create symbolic link of $DEPLOY_ROOT"/"$APP_NAME to $DEPLOY_ROOT/current"
unlink $CURRENT; ln -s $DEPLOY_ROOT"/"$APP_NAME $CURRENT;
echo "3) done"
#4 reload unicorn
echo "4) reload unicorn server"
sh /etc/init.d/unicorn_prd_init stop
echo "4) done"

echo "New version deployed!"
