#! /bin/sh
#This a template to make a cron job for gif sync from different websites.
#set up the job execution environment

export RAILS_ENV=production #can be production / integration / test
#export USER=deployprd

function set_up_env(){
    if [ ! -f ~/set_rbenv.$USER ]; then
        echo "Error : Can't find ruby env setup source file" >&2
        exit 1
    fi
    source ~/set_rbenv.$USER
}

function execute_synchronization(){
    #set_up_env
    project_root="/srv"
    cd $project_root
    rake gif_grabbers:imgur_front_page
}


execute_synchronization

