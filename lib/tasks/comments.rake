namespace :comments do
    desc "Track comments which don't relate to any gif and user"
    task :trackOphanComments => :environment do
        comments = Comment.all
        comments.each do |comment|  
            if comment.gif_id.nil? 
                p "============ comment with id : #{comment.id} is orphan(no associated gif)"
            else
                if Gif.where(id:comment.gif_id).empty?
                    p "============ comment with id : #{comment.id} is orphan(associated gif don't exist anymore)"
                end
                if User.where(id:comment.user_id).empty?
                    p "============ comment with id : #{comment.id} is orphan(associated user don't exist anymore)"
                end
            end    
        end  
    end
    
    desc "delete comments which don't relate to any gif and user"
    task :deleteOphanComments => :environment do
        comments = Comment.all
        comments.each do |comment|  
            if comment.gif_id.nil? 
                p "============ comment with id : #{comment.id} is orphan(no associated gif)"
                comment.destroy
            else
                if Gif.where(id:comment.gif_id).empty?
                    p "============ comment with id : #{comment.id} is orphan(associated gif don't exist anymore)"
                    comment.destroy
                end
                if User.where(id:comment.user_id).empty?
                    p "============ comment with id : #{comment.id} is orphan(associated user don't exist anymore)"
                    comment.destroy
                end
            end    
        end  
    end
    
end
