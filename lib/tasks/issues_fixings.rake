namespace :issues_fixings do
   desc "Fixing issue where imgur remove it's nsfw tag in json"
#{"id":47230251,"hash":"oZMWbcG","account_id":null,"account_url":null,"title":"MRW I was a little kid and a letter in the mail addressed me as #\"Sir\"","score":1640,"starting_score":1042,"virality":2588.4665856431,"size":761692,"views":170841,"is_hot":true,"is_album":false,"album_cover":null,"album_cover_width":0,"album_cover_height":0,"mimetype":"image\/gif","ext":#".gif","width":245,"height":300,"animated":true,"ups":522,"downs":9,"points":513,"reddit":"\/r\/reactiongifs\/comments\/2fstt6\/mrw_i_was_a_little_kid_and_a_letter_in_the_mail\/","bandwidth":"121.19 GB","timestamp":"2014-09-#08 11:56:11","hot_datetime":"2014-09-08 19:27:40","create_datetime":"2014-09-08 11:56:11","section":"reactiongifs","description":"","tags":[],"subtype":null,"pending":"0","favorited":false,"vote":null,"nsfw_tag":false}
#set all nsfw null value to true
  task :fix_imgur_remove_nsfw_tag_issue => :environment do
      Gif.where(:nsfw => nil).each do |gif|
        p "gif with name : #{gif.name} have error"
        gif.nsfw = false
        gif.save
      end
  end     

  desc "Fixing issue to giphy duplicates"
  task :deduplicate_giphy_images_base_on_name => :environment do
  	@gifs_all = Gif.all
  	# find all models and group them on keys which should be common
    grouped = @gifs_all.group_by{|gif| [gif.name] }
    grouped.values.each do |duplicates|
      # the first one we want to keep right?
      if duplicates.size > 1
	      first_one = duplicates.shift # or pop for last one
	      # if there are any more left, they are duplicates
	      # so delete all of them
	      p "=== dedup record #{first_one.name}"
	      duplicates.each{|double| double.destroy} # duplicates can now be destroyed
	  end    
    end
  end	
end
