namespace :users do
    desc "normalize user name in users table : only accept letters,numbers and _-"
    task :normalizeUsernames => :environment do
        users = User.all 
        users.each do |user|
            user_with_auth=Authorization.where(user_id:user.id)
            
            if /\A[0-9a-zA-Z_-]+\z/.match(user.username).nil? #check format
                p "user with id = #{user.id}, name = #{user.username} will be normalized!"
                user.username = user.username.gsub(/[^0-9a-zA-Z_-]/i, '_')
                user.save
            end
            
        end
    end
    
    desc "list the users who will be impacted by the user name normalisation"
    task :listImpactedUsersByNormalisation => :environment do
        users = User.all 
        users.each do |user|
            user_with_auth=Authorization.where(user_id:user.id)
            if user_with_auth.empty?
                if /\A[0-9a-zA-Z_-]+\z/.match(user.username).nil? #check format
                    p "user with id = #{user.id}, name = #{user.username} will be impacted!"
                end
            end
        end
    end
end
