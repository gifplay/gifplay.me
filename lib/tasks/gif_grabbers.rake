namespace :gif_grabbers do
    desc "Grab from imgur/r/nsfw_gif, execute once or daily CRON!"
    task :imgur_nsfw_gif => :environment do
        #create logger
        file = File.open("#{Rails.root}/log/imgur_r_nsfw_gif.log", File::WRONLY | File::APPEND | File::CREAT)
        logger = Logger.new(file)
        logger.formatter = proc do |severity, datetime, progname, msg|
            "[#{datetime}] #{severity} -- : #{msg}\n"
        end
        logger.info("Start imgur_nsfw_gif grabber")
        base_url = "http://imgur.com/r/nsfw_gif/new/page"
        #base_url = "http://imgur.com/r/nsfw_gif/top/all/page/"
        tag = Tag.find_or_create_by_name(name: '#nsfw_gif') do |t|
            t.user = User.where(id:2).first #anon user 
        end
        grab_imgur_gallery base_url, tag.name, logger
        
    end
    
    desc "Grab from imgur/r/gif, execute once or daily CRON!"
    task :imgur_r_gif => :environment do
        #create logger
        file = File.open("#{Rails.root}/log/imgur_r_gif.log", File::WRONLY | File::APPEND | File::CREAT)
        logger = Logger.new(file)
        logger.formatter = proc do |severity, datetime, progname, msg|
            "[#{datetime}] #{severity} -- : #{msg}\n"
        end
        logger.info("Start imgur_r_gif grabber")
        base_url = "http://imgur.com/r/gif/new/page"
        #base_url = "http://imgur.com/r/nsfw_gif/top/all/page/"
        tag = Tag.find_or_create_by_name(name: '#r_gif') do |t|
            t.user = User.where(id:2).first #anon user 
        end
        grab_imgur_gallery base_url, tag.name, logger
        
    end
    
    
    desc "Grab from front page, execute once or daily CRON!"
    task :imgur_front_page => :environment do
        #create logger
        file = File.open("#{Rails.root}/log/imgur_front_page.log", File::WRONLY | File::APPEND | File::CREAT)
        logger = Logger.new(file)
        logger.formatter = proc do |severity, datetime, progname, msg|
            "[#{datetime}] #{severity} -- : #{msg}\n"
        end
        logger.info("Start imgur_front_page grabber")
        base_url = "http://imgur.com/gallery/hot/time/day/page"
        #base_url = "http://imgur.com/r/nsfw_gif/top/all/page/"
        tag = Tag.find_or_create_by_name(name: '#imgur_front_page') do |t|
            t.user = User.where(id:2).first #anon user 
        end
        grab_imgur_gallery base_url, tag.name, logger
        
    end
    #Grab the entire imgur gallery with a base url
    def grab_imgur_gallery base_url, tag_name, logger
        #apparently on imgur public API MAX page is 50
        page_number = 0
        duplicated_entry = 0
        logger.info("Start grabber on #{base_url}")
        50.times do 
            request_url = "#{base_url}/#{page_number}.json"
            page_number = page_number + 1
            uri =  URI.parse request_url
            resp =  Net::HTTP.post_form(uri, {})
            #json_data = JSON.load(open(request_url))
            json_data = JSON.parse(resp.body)
            logger.info("Grab page : #{request_url}")
            image_data = json_data['data']
            image_data.each do |img_json|
                if img_json["animated"] # test if it is an animated gif
                    hash = img_json['hash']
                    if Gif.where("name" => hash).empty? #No gif with this hash is found in DB
                        #source_url = "http://i.imgur.com/#{hash}#{img['ext']}"
                        create_gif_entry img_json, tag_name,logger
                        sleep 10
                    else
                        duplicated_entry = duplicated_entry + 1
                    end
                    if duplicated_entry > 25 #more then 25 duplicate entry
                        logger.info("Grabber stop on : #{request_url}, end of the update")
                        return
                    end
                end    
            end
        end
    end
    #create gif with url, hash and title information
    #Imgur JSON data model
    #{"hash"=>"1n0t6nz", "title"=>"Bouncy", "author"=>"pepsi_next", "datetime"=>"2014-07-05 18:48:30", "ext"=>".gif", "mimetype"=>"image/gif", "views"=>3889, "bandwidth"=>"3.17 GB", "width"=>500, "height"=>266, "size"=>875795, "ups"=>4, "downs"=>0, "points"=>4, "permalink"=>"/r/NSFW_GIF/comments/29wwxc/bouncy/", "subreddit"=>"NSFW_GIF", "animated"=>"1", "nsfw"=>true, "created"=>1404586110, "score"=>1969, "date"=>"2014-07-05", "source"=>"http://i.imgur.com/pW469.gif", "description"=>nil, "favorited"=>false}
    def create_gif_entry img_json, tag_name, logger
        source_url = "http://i.imgur.com/#{img_json['hash']}#{img_json['ext']}"
        gif = Gif.new({"gif_url" => source_url})
        is_nsfw = img_json["nsfw"] || img_json["nsfw_tag"] #test two json tag
        gif.nsfw = is_nsfw.nil? ? true : is_nsfw #if is not specific then nsfw, otherwise use found value
        gif.published = 1
        gif.user = User.where(id:2).first #anon user
        gif.skip_generate_name = 1
        gif.overwrite_file_name img_json["hash"]
        gif.title= img_json["title"]
        #very bad tag management, to correct    
        gif.associate_tag_by_name tag_name
        if gif.nsfw
            gif.associate_tag_by_name "nsfw"
        end    
        logger.info("GET gif from #{source_url}, with title : #{gif.title}")
        puts("GET gif from #{source_url}, with title : #{gif.title}")
        gif.save
    end

    desc "Grab from giphy, execute once or daily CRON!"
    task :giphy => :environment do
        #create logger
        file = File.open("#{Rails.root}/log/giphy.log", File::WRONLY | File::APPEND | File::CREAT)
        logger = Logger.new(file)
        logger.formatter = proc do |severity, datetime, progname, msg|
            "[#{datetime}] #{severity} -- : #{msg}\n"
        end
        logger.info("Start giphy grabber")
        base_url = "http://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC"
        #base_url = "http://imgur.com/r/nsfw_gif/top/all/page/"
        tag = Tag.find_or_create_by_name(name: '#giphy') do |t|
            t.user = User.where(id:2).first #anon user 
        end
        grab_giphy base_url, tag.name, logger
        
    end
    
    #Grab the giphy gallery with a base url
    def grab_giphy request_url, tag_name, logger

        logger.info("Start grabber on #{request_url}")
        
        3.times do 
            begin 
                url = URI.parse(request_url)
                req = Net::HTTP::Get.new(url.to_s)
                res = Net::HTTP.start(url.host, url.port) {|http|
                  http.request(req)
                }
                json_data = JSON.parse(res.body)
                res = create_giphy_entry json_data, tag_name, logger
            end while res == false    
            
        end
    end
    
    def create_giphy_entry img_json, tag_name, logger

        source_url = img_json['data']['image_url']
        gif_name = img_json['data']['id']
        if Gif.exists?(name:gif_name) 
            logger.info("deug============== detect duplicated_entry #{gif_name}")
            puts("deug============== detect duplicated_entry #{gif_name}")
            return false
        else  
            gif = Gif.new({"gif_url" => source_url})
            gif.published = 1
            gif.nsfw = false
            gif.user = User.where(id:2).first #anon user
            gif.skip_generate_name = 1
            gif.overwrite_file_name gif_name
            gif.title= "user_upload"
            gif.associate_tag_by_name tag_name
            
            logger.info("GET gif from #{source_url}, with title : #{gif.title}")
            puts("GET gif from #{source_url}, with title : #{gif.title}")
            gif.save
            return true
        end    
    end
end

