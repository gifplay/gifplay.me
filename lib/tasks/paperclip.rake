#desactive new path in gif model 
namespace :paperclip do
    desc "paperclip task"
    task :migration_task => :environment do
        @gifs = Gif.all
        @gifs.each do |gif| 
            old_original_file_name = gif.upload.path(:original)
            if File.exists? old_original_file_name
                p "debug============ old original file place" + old_original_file_name
                new_original_file_name = Rails.root.join('public', 'i', gif.name, "original-"+gif.upload_file_name).to_s 
                FileUtils.mkdir_p(File.dirname(new_original_file_name))
                FileUtils.cp old_original_file_name , new_original_file_name #, :force    
                p "debug============ new originale file place : " + new_original_file_name
                
                old_thumb_file_name = gif.upload.path(:thumb)
                p "debug============ old thumb file place" + old_thumb_file_name
                new_thumb_file_name = Rails.root.join('public', 'i', gif.name, "thumb-"+gif.upload_file_name).to_s 
                FileUtils.mkdir_p(File.dirname(new_thumb_file_name))
                FileUtils.cp old_thumb_file_name , new_thumb_file_name #, :force
                p "debug============ new thumb file place : " + new_thumb_file_name
                #generate small sample thumb
                input_gif_file = new_original_file_name
                output_sample_file_dir = File.dirname(input_gif_file)
                output_sample_file = Rails.root.join(output_sample_file_dir,"sample-thumb-#{gif.name}.jpeg").to_s
                imageProcessCmd = "convert" #use imageMagick
                toExec = "#{imageProcessCmd} -coalesce -resize 100x100 \"#{input_gif_file}[0]\" #{output_sample_file}"
                system(toExec)
                p "debug============ exec command : #{toExec}" 
                p "debug============ new sample thumb file place : " + output_sample_file
            end
        end
    end
    
end
