namespace :gifs do
    desc "set nil values of views to 0 in the table gifs"
    task :fixViewsDefaultValues => :environment do
        @gifs = Gif.all
        @gifs.each do |gif| 
            if gif.views.nil?
                gif.views = 0
                gif.save
            end
        end
    end

    desc "set nil values of downvotes and upvotes to 0 in the table gifs"
    task :fixDownvotesUpvotesDefaultValues => :environment do
        @gifs = Gif.all
        @gifs.each do |gif| 
            if gif.upvotes.nil?
                gif.upvotes = 0
            end
            if gif.downvotes.nil?
                gif.downvotes = 0
            end
            gif.save
        end
    end

    desc "Track ophan gifs"
    task :TrackOphans => :environment do
        @gifs = Gif.all
        @gifs.each do |gif| 
            original_file_name = gif.upload.path(:original)
            if !File.exists? original_file_name
                p "debug =================== gif : #{gif.name} doesn't exist"
            end
            if User.where(id:gif.user_id).empty?
                "debug =================== gif : #{gif.name} is orphan(no associated users)"
            end
        end
    end
    desc "Delete ophan gifs"
    task :DeleteOphans => :environment do
        @gifs = Gif.all
        @gifs.each do |gif| 
            original_file_name = gif.upload.path(:original)
            if !File.exists? original_file_name
                p "debug =================== gif : #{gif.name} doesn't exist, will get deleted"
                gif.destroy
            end
        end
    end
    
    desc "set cors_enabled values to gifs according to it's source url"
    task :fixCorsEnabledDefaultValues => :environment do
        @gifs = Gif.all
        @gifs.each do |gif| 
            if gif.source_url != nil
                gif.cors_enabled = _cors_enabled? gif.source_url
                gif.save
                p "debug ========================= CORS is set as #{gif.cors_enabled} on #{gif.source_url}"
            end
        end
    end
    
    desc "find and save all gifs without source_url"
    task :migrateProjectSaveGifs => :environment do
        @gifs = Gif.all
        FileUtils.mkdir_p("/tmp/save")
        @gifs.each do |gif| 
            if gif.source_url == nil
                file_folder = Rails.root.join('public', 'i', gif.name).to_s 
                FileUtils.cp_r file_folder, "/tmp/save"
                p "debug============ save file_folder : " + file_folder
            end
        end
    end

    #check if CORS enabled for url source? see http://www.w3.org/TR/cors/
    def _cors_enabled? source_url
        uri = URI.parse source_url
        req = Net::HTTP::Head.new(uri.path)
        header = Net::HTTP.start(uri.host, uri.port,:use_ssl => uri.kind_of?(URI::HTTPS)) { |http| http.request(req)}
        #header.each { |k, v| puts "key : #{k}: Value :#{v}" }
        return (header["access-control-allow-origin"] == "*")
    end

    
    
end
