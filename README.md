# Vagrant env for gifplay.me

Simple environment setup using Vagrant and Chef to setup dev env

## Requirements

* Vagrant version latest.
* Last version of Virtualbox
* vagrant-librarian-chef-nochef

## Installation

`vagrant plugin install vagrant-librarian-chef-nochef`

## Usage

```bash
#In windows 
λ vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Checking if box 'ubuntu/trusty64' is up to date...
.... 
λ vagrant ssh
#In vagrant box
vagrant@vagrant-ubuntu-trusty-64:~$ cd /vagrant
vagrant@vagrant-ubuntu-trusty-64:~$ bundle install; rbenv rehash;
vagrant@vagrant-ubuntu-trusty-64:~$ rake db:migrate; rake db:seed;
```

* Vagrant will spin up a single virtual machine with neccessary tools for development

## Deployment
* Partiel deployment with sudo
```bash
deployprd@epicserver:~$ cd workspaces/
deployprd@epicserver:~/workspaces$ cd gifplay.me.v1.0.0/
#Stop container
deployprd@epicserver:~/workspaces/gifplay.me.v1.0.0$ docker-compose stop;
#remove container 
deployprd@epicserver:~/workspaces/gifplay.me.v1.0.0$ docker-compose rm;
#clean stopped container
deployprd@epicserver:~/workspaces/gifplay.me.v1.0.0$ docker rm `docker ps --no-trunc -aq`;
#clean non registed images
deployprd@epicserver:~/workspaces/gifplay.me.v1.0.0$ docker rmi $(docker images -q -f "dangling=true");
#build containers
deployprd@epicserver:~/workspaces/gifplay.me.v1.0.0$ docker-compose build
#start containers
deployprd@epicserver:~/workspaces/gifplay.me.v1.0.0$ docker-compose up -d
```

## Crontab restart
* with sudo
```bash
deployprd@epicserver:~$ cd workspaces/
deployprd@epicserver:~/workspaces$ cd gifplay.me.v1.0.0/
deployprd@epicserver:~/workspaces/gifplay.me.v1.0.0$ crontab config/gif_crawl_prod;

```