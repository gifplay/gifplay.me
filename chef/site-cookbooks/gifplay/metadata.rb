name             "gifplay"
maintainer       "MUG"
maintainer_email "exallion_long@hotmail.com"
license          "MIT"
description      "Manages gifplay installation."
version          "0.1.0"

depends 'mysql', '~> 6.0'
depends 'mysql2_chef_gem', '~> 1.0'
depends 'database'