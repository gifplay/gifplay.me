#
# Cookbook Name:: gifplay
# Recipe:: default
#
# Copyright 2015, MUG
#


class Chef::Recipe
  # mix in recipe helpers
  include Chef::RubyBuild::RecipeHelpers
end

mysql_service 'default' do
  version '5.6'
  bind_address '0.0.0.0'
  port '3306'  
  data_dir '/data'
  initial_root_password node['gifplay']['mysql']['initial_root_password']
  action [:create, :start]
end

mysql_client 'default' do
  action :create
end

mysql2_chef_gem 'default' do
  gem_version '0.3.17'
  action :install
end

mysql_connection_info = {:host => "127.0.0.1",
                         :username => 'root',
                         :password => node['gifplay']['mysql']['initial_root_password']}

mysql_database "gifplay" do
  connection mysql_connection_info
  action :create
end

mysql_database "gifplay_UT" do
  connection mysql_connection_info
  action :create
end

mysql_database_user 'gifplay' do
  connection mysql_connection_info
  password 'gifplay'
  action :create
end

mysql_database_user 'gifplay' do
  connection mysql_connection_info
  database_name 'gifplay'
  host '%'
  action :grant
end

mysql_database_user 'gifplay' do
  connection mysql_connection_info
  database_name 'gifplay_UT'
  host '%'
  action :grant
end

#execute "bundler-install" do
#  user "vagrant"
#  cwd "/vagrant"
#  command "bundler install"
#  action :run
#end