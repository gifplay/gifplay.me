# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # Use Ubuntu 14.04 Trusty Tahr 64-bit as our operating system
  config.vm.box = "ubuntu/trusty64"
  
  config.vm.hostname = "dev-gifplay.vagrant"

  # Configurate the virtual machine to use 2GB of RAM
  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory", "1024"]
    vb.customize ["modifyvm", :id, "--ioapic", "on"]
    vb.customize ["modifyvm", :id, "--cpus", "2"]   
  end

  # Forward the Rails server default port to the host
  config.vm.network :forwarded_port, guest: 3000, host: 4000
  
  config.librarian_chef.cheffile_dir = "chef"

  # Use Chef Solo to provision our virtual machine
  config.vm.provision :chef_solo do |chef|
    chef.cookbooks_path = ["chef/cookbooks", "chef/site-cookbooks"]
    chef.roles_path = "chef/roles"

    chef.add_recipe "apt"
    chef.add_recipe "ruby_build"
    chef.add_recipe "rbenv::user"
    chef.add_recipe "rbenv::vagrant"
    chef.add_recipe "vim"
  #  chef.add_recipe "gifplay"

    # Set an empty root password for MySQL to make things simple
    chef.json = {
      rbenv: {
        git_url: "https://github.com/sstephenson/rbenv.git",
        user_installs: [{
          user: 'vagrant',
          rubies: ["1.9.3-p547"],
          global: "1.9.3-p547"
        }]
      },
      bundler: {
        apps_path: "/vagrant"
      }
    }
  end
  config.vm.provision "shell", path: "chef/rails_bootstrap.sh", privileged: false
end
