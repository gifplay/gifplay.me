class FixGifSourceUrlColumnName < ActiveRecord::Migration
	#change the ambigue name or url to source_url
  def up
  	change_table :gifs do |t|
      t.rename :url, :source_url
    end
  end

  def down
  	change_table :gifs do |t|
      t.rename :source_url, :url
    end
  end
end
