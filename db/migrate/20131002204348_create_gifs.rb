class CreateGifs < ActiveRecord::Migration
  def change
    create_table :gifs do |t|     
      t.string :name
      t.integer :nbFrames
	  t.string :ext
	  t.string :title
	  t.string :description
	  t.integer :width
	  t.integer :height
	  t.integer :upvotes
	  t.integer :downvotes
	  t.integer :views
	  t.boolean :nsfw
	  t.string   "upload_file_name"
      t.string   "upload_content_type"
      t.integer  "upload_file_size"
      t.datetime "upload_updated_at"
      t.timestamps
    end
  end
end
