class ChangeRolesMaskToUsers < ActiveRecord::Migration
  def change
    change_column :users, :roles_mask,  :integer, :default =>"2"
  end
end
