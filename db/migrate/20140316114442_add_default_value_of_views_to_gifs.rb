class AddDefaultValueOfViewsToGifs < ActiveRecord::Migration
  #change the views column to 0 by default
  def change
  	change_column :gifs, :views,  :integer, :default =>0
  end
end
