class AddCorsEnabledToGifs < ActiveRecord::Migration
  
    def change
        add_column :gifs, :cors_enabled,  :boolean, :default =>false
    end
end
