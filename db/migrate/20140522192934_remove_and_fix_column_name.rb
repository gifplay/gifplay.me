class RemoveAndFixColumnName < ActiveRecord::Migration
  def up
  	remove_column :gifs, :ext
  	rename_column :gifs, :nbFrames, :nb_frames
  end

  def down
  end
end
