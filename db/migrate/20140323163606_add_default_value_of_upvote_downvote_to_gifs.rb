class AddDefaultValueOfUpvoteDownvoteToGifs < ActiveRecord::Migration
  def change
  	change_column :gifs, :upvotes,  :integer, :default =>0
  	change_column :gifs, :downvotes,  :integer, :default =>0
  end
end
