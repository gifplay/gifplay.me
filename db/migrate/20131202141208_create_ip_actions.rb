class CreateIpActions < ActiveRecord::Migration
  def change
    create_table :ip_actions do |t|
      t.string :ip_adr
      t.integer :user_id
      t.string :action_name

      t.timestamps
    end
  end
end
