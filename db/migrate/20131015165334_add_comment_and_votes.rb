class AddCommentAndVotes < ActiveRecord::Migration
  def change
    create_table(:votes) do |t|   	
      t.belongs_to :user
      t.belongs_to :gif 
      t.integer :note   
      t.timestamps      
    end
    
     create_table(:comments) do |t|   	
      t.belongs_to :user
      t.belongs_to :gif
      t.belongs_to :comment
      t.string   :commentaire
      t.timestamps      
    end
    
    change_table :votes do |t|
      t.belongs_to :comment  
    end
  end
end
