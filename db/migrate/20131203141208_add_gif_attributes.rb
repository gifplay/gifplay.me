class AddGifAttributes < ActiveRecord::Migration
  def change
    change_table :gifs do |t|
      	  t.string :url
	  	  t.integer :published
    end
  end
end
