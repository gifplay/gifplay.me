class AddTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :name
      t.string :description
      t.integer :howmany
      t.belongs_to :user
      t.timestamps
    end 
 
    create_table :gifs_tags do |t|
      t.belongs_to :tag
      t.belongs_to :gif
    end
  end
end
