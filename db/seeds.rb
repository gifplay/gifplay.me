# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

guest_user = User.create(username: "guest", email: "guest@guest.com", password:"guestguest")
guest_user.roles_mask = 0
guest_user.save!
admin_user = User.create(username: "admin", email: "admin@admin.com", password:"adminadmin")
admin_user.roles_mask = 8
admin_user.save!