class GifsOfTagController < GifsControllerTemplate
    # GET /t/[tag_name]
    # GET /t/[tag_name].json
    def index
        @tag = Tag.find("#"+params[:tag])
        if @tag.tag_type == "nsfw" 
            gifs_arr = @tag.gifs.where(:published => 1)
        else
            gifs_arr = @tag.gifs.accessible_by(current_ability)
        end
        @gifs = gifs_arr.page(params[:page]).per_page(18).order('id DESC')
        render_url = url_for :controller => self.controller_name, :action => self.action_name, :tag => params[:tag]
        respond_to do |format|
            if  ageChecked @tag.tag_type == "nsfw" 
                format.html # show.html.erb
            else
                format.html { redirect_to  :controller => 'special', :action => 'alert',:render_url => render_url}
            end
            format.json { render json: @gifs }
        end
    end
    # GET /t/:tag_name/:id
    # GET /t/:tag_name/:id.json
    def show
        @tag = Tag.find("#"+params[:tag])
        if @tag.tag_type == "nsfw" 
            gifs_of_tag_array = @tag.gifs.where(:published => 1)
        else
            gifs_of_tag_array = @tag.gifs.accessible_by(current_ability)
        end
        super gifs_of_tag_array
    end

end