class IpActionsController < ApplicationController
    #load_and_authorize_resource
    # GET /ip_actions
    # GET /ip_actions.json
    def index
        @ip_actions = IpAction.all

        respond_to do |format|
            format.html # index.html.erb
            format.json { render json: @ip_actions }
        end
    end
end