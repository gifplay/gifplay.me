class GifsController < GifsControllerTemplate
    #invoke Cancan, gifcontroller actions' permissions is defined with ability.rb, technical details see : https://github.com/ryanb/cancan
    load_and_authorize_resource
    #don't apply any rule is one has url
    skip_authorize_resource :only => [:latest, :random, :show , :iframe, :reverse, :navigate_left, :navigate_right]

    # GET /gifs
    # GET /gifs.json
    def index    	
        @gifs = @gifs.page(params[:page]).per_page(18).order('id DESC')
    end
    # GET /latest
    # GET /latest.json
    def latest
        gif=Gif.accessible_by(current_ability).mobile_gifs.last
        latest_gif_uri = URI.join(request.url, gif.upload.url(:original)).to_s
        respond_to do |format|
             format.html { redirect_to  gif.upload.url(:original) }
             format.json { render :json => {:gif_url => latest_gif_uri, 
                 :gif_details => gif.as_json(except: [:id,:user_id, :updated_at, :upload_updated_at]), 
                 :user_details => gif.user.as_json(only: [:username]),
                 :tags_details => gif.tags.as_json(:only => [:name]),
                 :status => status } }
        end
    end

    # GET /random
    # GET /random.json
    def random
        gifs=Gif.accessible_by(current_ability).mobile_gifs
        gif = _random_record gifs
        random_gif_uri = URI.join(request.url, gif.upload.url(:original)).to_s
        respond_to do |format|
             format.html { redirect_to  gif.upload.url(:original) }
             format.json { render :json => {:gif_url => random_gif_uri, 
                 :gif_details => gif.as_json(except: [:id,:user_id, :updated_at, :upload_updated_at]), 
                 :user_details => gif.user.as_json(only: [:username]),
                 :tags_details => gif.tags.as_json(:only => [:name]),
                 :status => status } }
        end
    end
    #GET /gifs/navigate_right/:gif_offset
    def navigate_right
        #get the offset of the current gif to compute where is the last page
        @gif_offset = params[:gif_offset].to_i - Gifgovern::CarouselPageSize
        @gif_offset = @gif_offset < 0 ?  0 : @gif_offset
        #@gifs_nav = Gif.accessible_by(current_ability).limit(Gifgovern::CarouselPageSize).offset(@gif_offset)
        @gifs_nav = _get_carousel_page Gif.accessible_by(current_ability), @gif_offset
        render :partial => "gifs_renders/navigate_gif", :locals => { :gifs_nav => @gifs_nav }, :layout => false, :status => 200
    end

    #GET /gifs/navigate_left/:gif_offset
    def  navigate_left
        #get the offset of the current gif to compute where is the next page
        @gif_offset = params[:gif_offset].to_i + Gifgovern::CarouselPageSize
        #cache it!!!!
        gifs_arr = Gif.accessible_by(current_ability)
        offset_limit = gifs_arr.count
        @gif_offset = @gif_offset > offset_limit ? offset_limit : @gif_offset
        @gifs_nav = _get_carousel_page gifs_arr, @gif_offset
        render :partial => "gifs_renders/navigate_gif", :locals => { :gifs_nav => @gifs_nav }, :layout => false, :status => 200
    end

    #Get /gifs/iframe/:id
    def iframe
        flash[:notice] = params[:notice]
        @gif = Gif.find(params[:id])
        @gif.views = !@gif.views.nil? ? @gif.views + 1 : 1
        @gif.save

        respond_to do |format|
            if  ageChecked @gif.nsfw
                format.html { render :layout => false}
            else
                format.html { redirect_to  :controller => 'special', :action => 'alert',:id => @gif.name}
            end
            format.json { render json: @gif }
        end
    end

    # GET /gifs/:id
    # GET /gifs/:id.json
    def show
        gifs_arr = Gif.accessible_by(current_ability)
        super gifs_arr
    end
    #Get /gifs/reverse/:id
    def reverse
        @gif = Gif.find(params[:id])
        respond_to do |format|
            if  ageChecked @gif.nsfw
                format.html { redirect_to  @gif.upload_reverse_url}
            else
                format.html { redirect_to  :controller => 'special', :action => 'alert',:id => @gif.name}
            end
            format.json { render json: @gif.upload_reverse_url }
        end
    end
    
    # GET /gifs/new
    # GET /gifs/new.json
    def new
        @gif = Gif.new
        respond_to do |format|
            format.html # new.html.erb
            format.json { render json: @gif }
        end
    end

    # GET /gifs/:id/edit
    def edit
        flash[:notice] = params[:notice]
        @gif = Gif.find_by_name(params[:id])
        @tags = Tag.all
        render_url = url_for :controller => self.controller_name, :action => self.action_name, :id => @gif.name
        respond_to do |format|
            if  ageChecked @gif.nsfw
                format.html # show.html.erb
            else
                format.html { redirect_to  :controller => 'special', :action => 'alert',:render_url => render_url}
            end
            format.json { render json: @gif }
        end

    end

    # POST /gifs
    # POST /gifs.json
    def create
        #this is the way to call the method in the helper
        action_permission = view_context.check_request_ip_permission(request.remote_ip,'POST')
        if action_permission
            @gif = Gif.new(params[:gif])
            @gif.nsfw = 0
            @gif.published = 0
            @gif.user = current_user || User.where(username:"guest").first || User.where(id:2).first 
            respond_to do |format|
                if @gif.save
                    ip_adr = request.remote_ip
                    @ip_action = IpAction.new(:ip_adr => ip_adr, :user_id => @gif.user.id, :action_name => 'POST')
                    @ip_action.save!
                    format.html {
                        render :json => [@gif.to_jq_upload].to_json,
                        :content_type => 'text/html',
                        :layout => false
                        }
                    format.json { render json: @gif.to_jq_upload, status: :created, location: @gif }
                else
                    format.html { render :json => @gif.errors[:upload], status: :unprocessable_entity,  :content_type => 'text/html',
                        :layout => false}
                    format.json { render json: @gif.errors[:upload], status: :unprocessable_entity }
                end
            end
        else
            respond_to do |format|
                format.html  { redirect_to  :controller => 'gifs', :action => 'index', notice: 'error.' }
            end
        end
    end

    # PUT /gifs/:id
    # PUT /gifs/:id.json
    def update
        hashtags = params[:gif][:description].to_s.scan(/#\S+/)
        if (params[:gif][:nsfw].to_i == 1 )
            hashtags << '#nsfw'
        end
        #parsing every hashtag
        for ht in hashtags.uniq
            #p "debug =============== current user #{current_user}"
            if !current_user.nil?
                tag = Tag.find_or_create_by_name(name: ht) do |t|
                    t.user = current_user
                end
                #associate tag and the gif
                @gif.associate_tag_by_name tag.name 
            end
        end    
        respond_to do |format|
            if @gif.update_attributes(params[:gif])
                format.html { redirect_to  :controller => 'gifs', :action => 'show', :id => @gif.name }
                format.json { head :no_content }
            else
                format.html { render json: @gif.errors, status: :unprocessable_entity }
                format.json { render json: @gif.errors, status: :unprocessable_entity }
            end
        end
    end

    # DELETE /gifs/1
    # DELETE /gifs/1.json
    def destroy
       super
    end

end
