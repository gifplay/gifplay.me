class VotesController < ApplicationController
    #invoke Cancan, VotesController actions' permissions is defined with ability.rb, technical details see : https://github.com/ryanb/cancan
    load_and_authorize_resource

    # POST /votes
    # POST /votes.json
    def create    
        gif= Gif.find(params[:gif_name]) 
        @vote= Vote.where('user_id =? and gif_id = ?',current_user.id, gif.id ).first 
        if  @vote.nil?
            @vote = Vote.new
            @vote.user_id = current_user.id
            @vote.gif_id = gif.id
            if @vote.note.to_i == 0    
                gif.upvotes = gif.upvotes.nil? ? 1 : gif.upvotes + 1
            else
                gif.downvotes = gif.downvotes.nil? ? 1 : gif.downvotes + 1
            end  
            gif.save
            @vote.save
        end
        respond_to do |format|   
            if @vote.save     	
                format.html { render :text  => "#{gif.upvotes.to_s} likes"  } 
            end
        end
    end
end
