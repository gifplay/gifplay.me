#Registration controller define and overwrite some default action in Device gem, more about device : https://github.com/plataformatec/devise
class RegistrationsController < Devise::RegistrationsController
    #This action overwrite the original action for the user creation, here we take into account the captcha(recaptcha gem https://github.com/ambethia/recaptcha) result.
    #technical details see:  https://github.com/plataformatec/devise/wiki/How-To:-Use-Recaptcha-with-Devise
    def create
        if !verify_recaptcha
            flash.delete :recaptcha_error
            build_resource
            resource.valid?
            resource.errors.add(:base, "There was an error with the recaptcha code below. Please re-enter the code.")
            clean_up_passwords(resource)
            respond_with_navigational(resource) { render :new }
        else
            flash.delete :recaptcha_error
            super
        end
    end

    def clean_up_passwords(*args)
        # Delete or comment out this method to prevent the password fields from
        # repopulating after a failed registration
    end
    #This action overwrite the default action for user update, no password update when it is left blanck
    def update
        account_update_params = params[:user]
        # remove the virtual current_password attribute update_without_password
        # doesn't know how to ignore it
        account_update_params.delete(:current_password)

        # required for settings form to submit when password is left blank
        if account_update_params[:password].blank?
            account_update_params.delete("password")
            account_update_params.delete("password_confirmation")
        end

        @user = User.find(current_user.id)
        if @user.update_attributes(account_update_params)
            set_flash_message :notice, :updated
            # Sign in the user bypassing validation in case his password changed
            sign_in @user, :bypass => true
            redirect_to after_update_path_for(@user)
        else
            render "edit"
        end
    end
    #overwrite the hard delete behavious, soft delete user; it won't cause problem as gifs can be ophaned
    def destroy
        #soft delete define in User model
        resource.soft_delete
        Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
        set_flash_message :notice, :destroyed if is_navigational_format?
        respond_with_navigational(resource){ redirect_to after_sign_out_path_for(resource_name) }
      end

end