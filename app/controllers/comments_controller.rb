class CommentsController < ApplicationController  
    #invoke Cancan, CommentsController's actions' permissions is defined with ability.rb, technical details see : https://github.com/ryanb/cancan
    load_and_authorize_resource

    # GET /comments/1
    # GET /comments/1.json
    def show
        @comment = Comment.find(params[:id])

        respond_to do |format|
            format.html # show.html.erb
            format.json { render json: @comment }
        end
    end

    # GET /comments/new
    # GET /comments/new.json
    def new
        @comment = Comment.new

        respond_to do |format|
            format.html # new.html.erb
            format.json { render json: @comment }
        end
    end

    # GET /comments/1/edit
    def edit
        @comment = Comment.find(params[:id])
    end

    # POST /comments
    # POST /comments.json
    def create    
        @comment = Comment.new(params[:comment])
        @comment.user=current_user
        respond_to do |format|
            if @comment.save
                #format.html { redirect_to :controller => 'gifs', :action => 'show', :id => @comment.gif.name,  notice: 'Comment created.' }
                #format.json { render json: @comment, status: :created, location: @comment }      
                format.html { render :partial => "comments/comment_render", :locals => { :comment => @comment }, :layout => false, :status => :created }
            end
        end
    end


    def index
        @comment = Comment.all

        respond_to do |format|
            format.html # index.html.erb
            format.json { render json: @comment }
        end
    end

    # PUT /comments/1
    # PUT /comments/1.json
    def update
        @comment = Comment.find(params[:id])

        respond_to do |format|
            if @comment.update_attributes(params[:comment])
                format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
                format.json { head :no_content }
            else
                format.html { render action: "edit" }
                format.json { render json: @comment.errors, status: :unprocessable_entity }
            end
        end
    end

    # DELETE /comments/1
    # DELETE /comments/1.json
    def destroy
        @comment = Comment.find(params[:id])
        @comment.destroy

        respond_to do |format|
            format.html { redirect_to :controller => 'comments', :action => 'index', notice: 'Comment deleted.' }
            format.json { head :no_content }
        end
    end
end
