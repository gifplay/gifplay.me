class TagsController < ApplicationController

    #invoke Cancan, TagsController actions' permissions is defined with ability.rb, technical details see : https://github.com/ryanb/cancan
    load_and_authorize_resource
    # GET /tags
    # GET /tags.json
    def index
        @tags = Tag.all
        @grouped = {}
        @gifs = Gif.accessible_by(current_ability).order('views DESC').limit(27)	
        @tags.each do |company|
            letter = company.name.slice(1,1).upcase
            @grouped[letter] ||= []
            @grouped[letter] << company
        end

        respond_to do |format|
            format.html # index.html.erb
            format.json { render json: @tags }
        end
    end

    # GET /tags/1/edit
    def edit
        @tag = Tag.find(params[:id])
    end

    # POST /tags
    # POST /tags.json
    def create
        @tag = Tag.new(params[:tag])

        respond_to do |format|
            if @tag.save
                format.html { redirect_to '/tags', notice: 'Tag was successfully created.' }
                format.json { render json: @tag, status: :created, location: @tag }
            else
                format.html { render action: "new" }
                format.json { render json: @tag.errors, status: :unprocessable_entity }
            end
        end
    end

    # PUT /tags/1
    # PUT /tags/1.json
    def update
        @tag = Tag.find(params[:id])

        respond_to do |format|
            if @tag.update_attributes(params[:tag])
                format.html { redirect_to '/tags', notice: 'Tag was successfully updated.' }
                format.json { head :no_content }
            else
                format.html { render action: "edit" }
                format.json { render json: @tag.errors, status: :unprocessable_entity }
            end
        end
    end

    # DELETE /tags/1
    # DELETE /tags/1.json
    def destroy
        @tag = Tag.find(params[:id])
        @tag.destroy

        respond_to do |format|
            format.html { redirect_to tags_url }
            format.json { head :no_content }
        end
    end
end
