class GifsControllerTemplate < ApplicationController

    # Index methode to be overwriten 
    def index
        "Index method to be overwriten"
    end

    #show take an array of gifs
    def show gifs_array_param
        flash[:notice] = params[:notice]
        @gif = Gif.find(params[:id])
        @gif_offset = gifs_array_param.count(:conditions => ["gifs.id < ?",@gif.id])
        #offset is (gif_position - 3) to make the gif appear to be at 3rd position in carousel
        @gifs_nav = _get_carousel_page gifs_array_param, @gif_offset
        #-------------- PREVOUS -- RANDOM -- NEXT ----------------
        #in case of isolate url
        gif_position = @gifs_nav.index(@gif).nil? ? 3 : @gifs_nav.index(@gif)
        @gifRandom = @gifs_nav[0].name#_random_record gifs_array_param
        #get the next gif position, prevent index out of bound
        next_gif_position = (gif_position + 1) >= @gifs_nav.size ? @gifs_nav.size-1 : gif_position + 1
        @gifNext = @gifs_nav[next_gif_position].name
        #get the previous gif position, prevent index out of bound
        prev_gif_position = (gif_position - 1) < 0 ? 0 : gif_position -1

        @gifPrevious = @gifs_nav[prev_gif_position].name
        #---------------------------------------------------------
        @gif.views = @gif.views + 1 
        @gif.save
        render_url = url_for :controller => self.controller_name, :action => self.action_name, :id => @gif.name
        respond_to do |format|
            if  ageChecked @gif.nsfw
                format.html # show.html.erb
            else
                format.html { redirect_to  :controller => 'special', :action => 'alert',:render_url => render_url}
            end
            format.json { render json: @gif }
        end
    end

    def _random_record record_array_param
        record_array_param.sample
    end

    #private method, take an array of gifs, and the offset of the current gif, return a window of gifs for carousel
    def _get_carousel_page gifs_arr_param, gif_offset_param
        gif_arr_size = gifs_arr_param.count
        gif_offset = gif_offset_param - 3 < 0 ? 0 : gif_offset_param -3
        gifs_nav = gifs_arr_param.limit(Gifgovern::CarouselPageSize).offset(gif_offset)
        missing_gif = Gifgovern::CarouselPageSize - gifs_nav.size
        missing_gif.times{
            gifs_nav.push _random_record gifs_arr_param
            }
        #re-order the gifnave from the higher id to the lower
        return gifs_nav.reverse
    end


    def ageChecked nsfw
        return (cookies[:age] == "over 21" or !nsfw)?  true  :  false
    end
    
    # DELETE /gifs/1
    # DELETE /gifs/1.json
    def destroy
        Gif.find(params[:id]).destroy
        respond_to do |format|
            format.html { redirect_to gifs_url }
            format.json { head :no_content }
        end
    end

end