class SpecialController < ApplicationController

    #load_and_authorize_resource
    def howarwe    
        respond_to do |format|
            format.html    
        end
    end

    def termsOfServices    
        respond_to do |format|
            format.html    
        end
    end

    def howto    
        respond_to do |format|
            format.html    
        end
    end

    def alert   
        respond_to do |format|
            format.html    
        end
    end
    
    #GET /player?play=
    #player_error_status : 0 = success , -1 = unknown error , 6997 = source url not supplied , 5987 = source url is not a gif, 9973 = NO CORS support on the source URL
    def player
        @source_url = params[:play] || "" #eliminate nil
        @player_error_status = 0 #default success
        #p "debug================ #{@source_url}" 
        uri_host = URI.parse(@source_url).host
        if uri_host ==  "imgur.com" #test uri host
            @source_url =  _parse_imgur_url @source_url    
        end    
        #source url not supplied
        if @source_url == nil || @source_url =="" #source url not supplied
            @player_error_status = 6997 #last prime number before 7000 :-) 
        elsif FastImage.type(@source_url) == :gif #source url is a gif(get out of if/else)
            @player_error_status = 0   
        else   #source url is not a gif
            @player_error_status = 5987 #source url is not a gif
        end
        if @player_error_status == 0 #URL is valided 
            if !_cors_enabled? @source_url #NO CORS support
                @player_error_status = 9973 #Last prime under 10000
            end
        end
        p "debug================ player_error_status : #{@player_error_status}"
        respond_to do |format|
            format.html
        end
    end

    #check if CORS enabled for url? see http://www.w3.org/TR/cors/
    def _cors_enabled? valide_source_url
        uri = URI.parse valide_source_url
        req = Net::HTTP::Head.new(uri.path)
        header = Net::HTTP.start(uri.host, uri.port,:use_ssl => uri.kind_of?(URI::HTTPS)) { |http| http.request(req)}
        #header.each { |k, v| puts "key : #{k}: Value :#{v}" }
        return (header["access-control-allow-origin"] == "*")
    end
    
    #Parse imgur url
    def _parse_imgur_url source_url
        imgur_path= URI(source_url).path
        imgur_hash = imgur_path.split("/").last
        imgur_url = "http://i.imgur.com/#{imgur_hash}.gif"
    end
    

    def redirection  # user said he is more than 21 years old
        cookies[:age] = { value: "over 21", expires: 30.day.from_now }
        respond_to do |format|
            #puts "\n\n"+params[:render_url]
            format.html { redirect_to  params[:render_url] }
        end
    end

    def userspace  
        @score = 0    	
        @views = 0
        @upvotes = 0
        @user = User.where(username: params[:username]).first #of course user name is unique
        @comment = Comment.where(user_id: @user.id)
        #score calcul shoud be base on the gifs belongs to the user and be fix
        gif_to_scores = Gif.where(user_id:@user.id)
        if current_user == @user
            #show all the gifs belongs to the user 
            @gifs = gif_to_scores.page(params[:page]).per_page(18).order('id DESC')
        else
            #get the gif which belongs to user, and show them according to the current user abiliby
            @gifs = gif_to_scores.accessible_by(current_ability).page(params[:page]).per_page(18).order('id DESC')
        end
        @uploaded = gif_to_scores.size
        gif_to_scores.each do |g|
            @score = @score + 100 
            @views = @views + (g.views || 0 ) 
            @upvotes = @upvotes + (g.upvotes || 0 )  
        end
        @score = @score +@views + 10 * @upvotes
        respond_to do |format|
            format.html    
        end
    end


end