module GifsHelper
  def check_request_ip_permission(ip_adr,action)
    action_performed = IpAction.action_performed_in_the_past_inteval?(ip_adr,action,7)
    if action_performed == true #post action performed last 7 seconde so not now!
      return false
    end
    last_24h_actions =IpAction.count_last_24h_actions(ip_adr,action)
    if last_24h_actions >= 100
      return false
    end
    return true
  end
end