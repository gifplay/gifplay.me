class Authorization < ActiveRecord::Base
  belongs_to :user

  def fetch_details
    self.send("fetch_details_from_#{self.provider.downcase}")
  end
end