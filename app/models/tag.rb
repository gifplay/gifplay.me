class Tag  < ActiveRecord::Base
    #FriendId gem, make tag name as ID in the tags table, see the details on https://github.com/norman/friendly_id/
    extend FriendlyId
    friendly_id :name

    attr_accessible :description,:name,:howmany,:user, :tag_type
    belongs_to :user
    has_and_belongs_to_many :gifs

    def to_param
        "#{self.name}"
    end

end
