class Comment < ActiveRecord::Base
    attr_accessible :commentaire,:gif,:gif_name
    belongs_to :user
    belongs_to :gif
    belongs_to :comment
    
    validates_length_of :commentaire, :minimum => 2, :maximum => 150, :allow_blank => false
    
    #use virtual attribute 
    def gif_name=(name)
        #use find_by_name due to a strange issue caused by FriendlyID
        self.gif=Gif.find_by_name(name)
    end
    
end
