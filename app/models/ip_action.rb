class IpAction < ActiveRecord::Base
  attr_accessible :action_name, :ip_adr, :user_id

  def self.count_last_24h_actions(ip_adr,action_name)
    #count(:conditions=>['starttime>=?',Time.now-3600*24])
    count(:conditions=>{action_name: action_name, ip_adr: ip_adr, created_at: (Time.now - 1.day)..Time.now})
  end

  def self.count_last_1h_actions(ip_adr,action_name)
    count(:conditions=>{action_name: action_name, ip_adr: ip_adr, created_at: (Time.now - 1.hour)..Time.now})
  end
  #inteval defined in seconds
  def self.action_performed_in_the_past_inteval?(ip_adr,action_name,inteval)
    res = count(:conditions=>{action_name: action_name, ip_adr: ip_adr, created_at: (Time.now - inteval.second)..Time.now})
    if res > 0
      return true
    end

    return false
  end
end