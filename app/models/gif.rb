class Gif < ActiveRecord::Base
    #FriendId gem, make image name as ID in the gif table, see the details on https://github.com/norman/friendly_id/
    extend FriendlyId
    friendly_id :name

    belongs_to :user
    has_many :comment
    has_many :vote
    has_and_belongs_to_many :tags
    attr_accessible :name, :nb_frames, :picture , :width, :height, :description, :nsfw, :title, :upvotes, :downvotes, :published, :source_url, :cors_enabled
    attr_accessible :upload
    #attached file access : upload.path(:original) or upload.path(:thumb)
    has_attached_file :upload,
    :styles => { :thumb => "100x100" },
    :path => ":rails_root/public/i/:basename/:style-:filename",
    :url => "/i/:basename/:style-:filename",
    :use_timestamp => false 

    #validate the presence of the upload image
    validates_attachment_presence :upload
    #validate the upload image content type
    validates_attachment_content_type :upload, :content_type => Gifgovern::ImageTypes, :message => "Only jpeg, gif and png files are allowed"
    #validate the upload image size
    validates_attachment_size :upload, :less_than => 15.megabytes, :message => "only the file size less than 15 megabytes are allowed"

    attr_accessible :gif_url #virtual attribute
    validate :_gif_url_must_parse 
    #define the virtual attribute use to make img use a given name, see "before_create :before_create_process, :unless => :skip_generate_name "
    attr_accessor :skip_generate_name  #virtual attribute
    
    #get the mobile gifs with it's constrain : to be gif and less than 4 MB, use by latest action in gifcontroller
    def self.mobile_gifs
        arel_t = self.arel_table
        Gif.where(arel_t[:upload_content_type].eq("image/gif").and(arel_t[:upload_file_size].lt(10.megabytes)))
    end

    include Rails.application.routes.url_helpers

    def to_jq_upload
        {
            "name" => read_attribute(:upload_file_name),
            "size" => read_attribute(:upload_file_size),
            "url" => upload.url(:original),
            "delete_url" => gif_path(self),
            "delete_type" => "DELETE" ,
            "show_url" => '/gifs/'+self.name,
            "edit_url" => "/gifs/#{self.name}/edit"
            }
    end

    before_create :before_create_process, :unless => :skip_generate_name    
    #cant't use after_create, otherwise in the post process we can't generate the sample thumb image from unsaved upload
    after_save :after_save_process
    
    #empty constructor
    def gif_url
    end
    #contructor with url as a parameter
    def gif_url=(url)
        begin
            @gif_url = URI.parse(url)
            resp = @gif_url.kind_of?(URI::HTTP||URI::HTTPS)
            if(resp == true)
                io = open(@gif_url)
                self.upload = io
                self.source_url = url #record source_url origin
                self.cors_enabled = _cors_enabled?
            end
        rescue Exception
            resp = false
            #@gif_url_parse_error = url + "is not an valid url"
        end
        unless resp == true
            @gif_url_parse_error = "there is a problem on your source url"
        end
    end
    
    #overwrite the random file name
    def overwrite_file_name given_name
        #split content type to extension : ['image/gif'] => gif
        content_type = upload_content_type.split('/')[1]
        base_name = given_name
        self.upload.instance_write(:file_name, "#{base_name}.#{content_type}")
        self.name = base_name
        #self.title = base_name
    end
    
    #Give the external original url, give the original url is it is cors supported
    def upload_original_url
        if self.cors_enabled
            self.source_url
        else
            self.upload.url(:original)
        end    
    end    
    
    #Give the external sample url 
    def upload_sample_url
        "/i/#{name}/sample-thumb-#{name}.jpeg"
    end 
    
    #Give the external reverse url 
    def upload_reverse_url
        original_file_type = FastImage.type(upload.path)
        res_url = "/i/#{name}/reverse-#{name}.gif"
        if original_file_type != :gif
            res_url = upload.url
        else
            if !File.exist? _upload_reverse_path #if reverse gif don't exist
                _generate_reverse_gif
            end
        end
        return res_url
    end 
    
    #get the original source of the image(local or url)
    def get_image_source
        if !self.source_url.blank?
            source_uri = URI.parse(self.source_url)
            uri_host = source_uri.host
            if uri_host.include? "imgur" #an imgur image!
                imgur_path= source_uri.path
                imgur_hash = imgur_path.split("/").last
                imgur_hash= imgur_hash.sub(/\.[^.]+$/, "") #remove the extension if exist
                return "http://imgur.com/#{imgur_hash}"
            else
                return self.source_url
            end    
        else
            return "#"
        end
    end
    
    
    #associate a tag to the gif, assume the tag with this name exists
    def associate_tag_by_name tag_name
        tag = Tag.where(name: tag_name).first
        if tag != nil #tag exist
            if !self.tags.include? tag #tag not already associated with gif
                tag.howmany = tag.howmany + 1
                tag.save!
                self.tags.push tag #association
            end    
        end    
    end
    
    #custom destroy , remove also generated sample 
    def destroy
        gif_folder = File.dirname(upload.path(:original))
        super
        FileUtils.rm_rf(gif_folder)
    end
    
##############################################Protected Methods start here ##################################################    
    protected 
    #get make a random file name before creation
    def before_create_process
        _randomize_file_name
    end
    #To generate sample thumbnail after the gif creation
    def after_save_process
        if (! File.exists? _upload_sample_path) and  File.exists? upload.path(:original) #check if the sample file already exists, avoid to repeat it when update calls
            input_gif_file = upload.path
            imageProcessCmd = "convert" #use imageMagick
            toExec = "#{imageProcessCmd} -coalesce -resize 100x100 \"#{input_gif_file}[0]\" #{_upload_sample_path}"
            system(toExec)
            _save_image_dimensions
            #self.save #update image dimension
        end
    end
    
##############################################Private Methods start here ##################################################      
    private
    #set a random file name, only internal use
    def _randomize_file_name
        #split content type to extension : ['image/gif'] => gif
        content_type = upload_content_type.split('/')[1]
        base_name = _generate_secure_random_str #basename start with a letter
        self.upload.instance_write(:file_name, "#{base_name}.#{content_type}")
        self.name = base_name
        #self.title = base_name
        self.title = Gifgovern::ImageDefaultName
    end
    
    #generate secure hash, internal use
    def _generate_secure_random_str
        size = 4 + SecureRandom.random_number(3) #=> 4,5,6 caracters
        secure_str = SecureRandom.urlsafe_base64(size).gsub(/[_-]/, '_' => 'x', '-' => 'z') #=> substitude specail caractere such as _ and -
    end

    #set the image dimension, use fast image solution, internal use
    def _save_image_dimensions
        size = FastImage.size upload.path(:original)
        update_attributes(:width => size[0].to_i, :height => size[1].to_i) 
    end

    #To raise url parsing error, internal use
    def _gif_url_must_parse
        if @gif_url_parse_error != nil
            self.errors[:upload] = @gif_url_parse_error
        end
    end

    #Give the absolute system path of the sample thumb file, only internal use, to get the rendered url use upload_sample_url
    def _upload_sample_path
        sample_file_dir = File.dirname(upload.path)
        sample_file_path = Rails.root.join(sample_file_dir,"sample-thumb-#{name}.jpeg").to_s
        return sample_file_path
    end
    
    
    #Give the absolute system path of the reverse gif file, only internal use, to get the rendered url use upload_reverse_url
    def _upload_reverse_path
        reverse_file_dir = File.dirname(upload.path)
        reverse_file_path = Rails.root.join(reverse_file_dir,"reverse-#{name}.gif").to_s
        return reverse_file_path
    end
    
    #generate the reverse gif to the correct path, internal use
    def _generate_reverse_gif
        input_gif_file = upload.path
        imageProcessCmd = "convert" #use imageMagick
        toExec = "#{imageProcessCmd} #{input_gif_file}  -coalesce -reverse -quiet -layers OptimizePlus -loop 0 #{_upload_reverse_path}"
        system(toExec)
    end
    
    #check if CORS enabled for url source? see http://www.w3.org/TR/cors/, internal use
    def _cors_enabled?
        uri = URI.parse self.source_url
        req = Net::HTTP::Head.new(uri.path)
        header = Net::HTTP.start(uri.host, uri.port,:use_ssl => uri.kind_of?(URI::HTTPS)) { |http| http.request(req)}
        #header.each { |k, v| puts "key : #{k}: Value :#{v}" }
        return (header["access-control-allow-origin"] == "*")
    end

end