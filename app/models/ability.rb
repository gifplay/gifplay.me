#Ability class define the user role over different action in the controllers, it is standard class required by Cancan gem, see https://github.com/ryanb/cancan for more details
class Ability
    include CanCan::Ability
    
    def initialize(user)
        #define the default user with 0 in role mask
        default_user = User.new
        default_user.roles_mask = 0
        user ||= default_user
        if user.role? :redpanda
            #admin can do everything
            can :manage, :all
        else
            if user.role?(:author)
                can :create, [Gif,Comment,Tag,Vote]
                #can :read, Gif, :user_id => user.id
                can :update, Gif do |gif|
                    gif.try(:user) == user || user.role?(:moderator)
                end
            end
            #everyone can read comments and tags and votes
            can :read, [Comment,Tag,Vote]
            #everyone can see published gifs
            can :read, Gif, :published => 1, :nsfw => false
            #everyone can create gif but can't publish
            can :create, Gif , :published => 0
            #everyone can update unpublished gif
            can :update, Gif, :published => 0
        end
    end
end
