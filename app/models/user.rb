class User < ActiveRecord::Base
    # Include default devise modules. Others available are:
    # :token_authenticatable, :confirmable,
    # :lockable, :timeoutable and :omniauthable
    # technical details see : https://github.com/plataformatec/devise#configuring-models
    devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable# , :confirmable

    # Setup accessible (or protected) attributes for your model
    attr_accessible :username, :email, :password, :password_confirmation, :remember_me
    # designed to integrate the social login
    devise :omniauthable, :omniauth_providers => [:google_oauth2,:facebook,:twitter,:yahoo]

    # Virtual attribute for authenticating by either username or email
    # This is in addition to a real persisted field like 'username'
    attr_accessor :login
    attr_accessible :login

    # attr_accessible :title, :body
    has_many :gif
    has_many :comment
    has_many :authorizations
    #validates_uniqueness_of :login

    validates :username,
    :uniqueness => { :case_sensitive => false }
    #,
    #:format => { :with => /\A[0-9a-zA-Z_-]+\z/,
    #:message => "Only letters numbers and - _ are allowed" }


    #This named scope takes a role as an argument and performs a bitwise operation on it to determine whether a
    #user belongs to that role. We can test this in the console by finding all of the users in the admin role.
    # >> User.with_role("admin")
    #Embedding a Many-to-Many role Relationship
    #details see http://asciicasts.com/episodes/189-embedded-association
    scope :with_role, lambda { |role| {:conditions => "roles_mask & #{2**ROLES.index(role.to_s)} > 0 "} }
    # anon(guest user without an acount ! in case to ban a user, downgrade him to guest) : 2⁰| author(normal user) : 2¹ |moderator : 2² |redpanda(admin) : 2³
    ROLES = %w[anon author moderator redpanda]

    def roles=(roles)
        self.roles_mask = (roles & ROLES).map { |r| 2**ROLES.index(r) }.sum
    end

    def roles
        ROLES.reject { |r| ((roles_mask || 0) & 2**ROLES.index(r)).zero? }
    end

    def role?(role)
        roles.include? role.to_s
    end


    #login with email OR login
    def self.find_first_by_auth_conditions(warden_conditions)
        conditions = warden_conditions.dup
        if login = conditions.delete(:login)
            where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
        else
            where(conditions).first
        end
    end

    def self.new_with_session(params,session)
        if session["devise.user_attributes"]
            new(session["devise.user_attributes"],without_protection: true) do |user|
                user.attributes = params
                user.valid?
            end
        else
            super
        end
    end

    # login with the social login
    def self.from_omniauth(auth, current_user)
        #check the provider with this uid already exist
        logger.info "AUTH INFO : #{auth.to_s}"
        authorization = Authorization.where(:provider => auth.provider, :uid => auth.uid.to_s).first_or_initialize
        if authorization.user.blank?
            #user = current_user.nil? ? User.where(:email => auth["info"]["email"]).first : current_user
            if !auth["info"]["email"].blank? #test if email is present
                user = User.where(:email => auth["info"]["email"]).first #test user with the same email exist already
                valid_user=true
            end
            if user.blank?
                user = User.new
                user.password = Devise.friendly_token[0,20]
                username = normalize_username(auth.info.name)
                #if user name already exists, give a suffix
                if !User.where('lower(username) = ?', username.downcase).empty?
                    username = "#{username}_#{SecureRandom.hex(4)}"
                end
                user.username = username
                user.email = auth.info.email
                #auth.provider == "twitter" ?  user.save(:validate => false) :  user.save
                valid_user ? user.save : user.save(:validate => false) 
            end
            authorization.auth_name = auth.info.nickname
            authorization.user = user
            authorization.save
        end
        authorization.user
    end
    
    #username only allow "0-9a-z_-"
    def self.normalize_username name
        return name.gsub(/[^0-9a-zA-Z_-]/i, '_')
    end

    #yeah fuck you, once registed u sell your soul
    def soft_delete
        # set role mask to 0
        update_attribute(:roles_mask, 0)
    end

end