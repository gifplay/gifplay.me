class Vote < ActiveRecord::Base     
    attr_accessible :note,  :user_id, :gif_id
    belongs_to :user
    belongs_to :gif   
end
