
/* credit to https://gist.github.com/qbraksa/5606187 I use his script */
var currentPage = 1;
var intervalID = -1000;

function checkScroll() {
    if (nearBottomOfPage()) {
        currentPage++;
        console.log("endless request "+ currentPage);
        jQuery.ajax('?page=' + currentPage, {asynchronous:true, evalScripts:true, method:'get', success: function(data, textStatus, jqXHR) {
            $('#gif_index').append(jQuery(data).find('#gif_index').html());
            if(typeof jQuery(data).find('#gif_index').html() == 'undefined' || jQuery(data).find('#gif_index').html().trim().length == 0){
                clearInterval(intervalID);
            }
        },});
    }
}

function nearBottomOfPage() {
    return scrollDistanceFromBottom() < 50;
}

function scrollDistanceFromBottom(argument) {
    return pageHeight() - (window.pageYOffset + self.innerHeight);
}

function pageHeight() {
    return Math.max(document.body.scrollHeight, document.body.offsetHeight);
}

function over(id){
    if (document.getElementById('minigifImg'+id).width != null){
        document.getElementById('mini'+id).style.display = 'none';
        document.getElementById('minigif'+id).style.display = 'block';
    }
}
function out(id){
    document.getElementById('minigif'+id).style.display = 'none';
    document.getElementById('mini'+id).style.display = 'block';
}