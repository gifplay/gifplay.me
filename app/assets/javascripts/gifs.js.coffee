# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
##########&&&&&&&&&&&&&&&&&& THIS IS THE BLACK MAQUIC &&&&&&&&&&&&&&&&&&&######################	 
localCache =
    data: {}
    remove: (url) -> 
        delete localCache.data[url]

    exist: (url) -> 
        localCache.data.hasOwnProperty(url) and localCache.data[url] isnt null
    
    get: (url) -> 
        console.log('Getting in cache for url' + url);
        localCache.data[url]
   
    set: (url, cachedData, callback) ->
        localCache.remove(url);
        localCache.data[url] = cachedData;
        if ($.isFunction(callback)) 
        	callback(cachedData);

jQuery ->
	data_treatment = (data) ->
		#alert "Successful AJAX call";
		$('.carousel-inner').empty();
		$('.carousel-inner').append("#{data}");

	$("#navigate_left").click ->
  		#alert 'navigate_left clicked';
  		#navigation_id_left=$('#navigate_left_id').attr("value");
  		navigation_offset = $('#navigation_offset').attr("value");
  		#navigation_type= $('#tag').attr("value").replace('#','%23');   		
  		url = '/gifs/navigate_left/'+navigation_offset;
  		$(document).ready ->
      			$.ajax url,
		        type: 'GET'
		        dataType: 'html'
		        cache: true,
		        beforeSend: ->
		        	if localCache.exist(url)
	                    data_treatment localCache.get(url)
	                    return false;
	                return true;   
		        error: (jqXHR, textStatus, errorThrown) ->
		            #$('body').append "AJAX Error: #{textStatus}"
		        success: (data, textStatus, jqXHR) ->
		        	localCache.set(url, data, data_treatment);
		            #alert "Successful AJAX call #{data}";
		            #$('.carousel-inner').empty();
		            #$('.carousel-inner').append("#{data}");

	$("#navigate_right").click ->
  		#alert 'navigate_right clicked';
  		#navigation_id_right=$('#navigate_right_id').attr("value");  		
  		#navigation_type= $('#tag').attr("value").replace('#','%23');  
  		navigation_offset = $('#navigation_offset').attr("value");
  		url = '/gifs/navigate_right/'+navigation_offset;
  		$(document).ready ->
      			$.ajax url,
		        type: 'GET'
		        dataType: 'html'
		        cache: true,
		        beforeSend: ->
		        	if localCache.exist(url)
	                    data_treatment localCache.get(url)
	                    return false;
	                return true;   
		        error: (jqXHR, textStatus, errorThrown) ->
		            #$('body').append "AJAX Error: #{textStatus}"
		        success: (data, textStatus, jqXHR) ->
		        	localCache.set(url, data, data_treatment);
		            #$('.carousel-inner').empty();
		            #$('.carousel-inner').append("#{data}");           	            
