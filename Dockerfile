#Dockerfile for Rails server
FROM ruby:1.9.3-p547

WORKDIR /tmp
RUN gem install foreman
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install

ADD . /srv
WORKDIR /srv
RUN bundle exec rake assets:clean; bundle exec rake assets:precompile

#CMD ["foreman", "start", "-f", "Procfile"]
CMD ["puma","-C","config/puma.rb"]
#CMD ["python","-m","SimpleHTTPServer"]
