#!/bin/bash

deployprd@epicserver:~$ cd workspaces/
deployprd@epicserver:~/workspaces$ cd gifplay.me.v1.0.0/

sudo docker-compose stop;

sudo docker-compose rm;

sudo docker rm `docker ps --no-trunc -aq`;

sudo docker rmi $(docker images -q -f "dangling=true");

sudo docker-compose build

sudo docker-compose up -d
